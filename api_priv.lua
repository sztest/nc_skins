-- LUALOCALS < ---------------------------------------------------------
local minetest
    = minetest
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

if minetest.settings:get_bool(modname .. "_nopriv") then
	function api.skin_allowed() return true end
	return
end

minetest.register_privilege(modname, {
		description = "Custom skin use allowed",
		give_to_singleplayer = true,
		give_to_admin = false
	})

function api.skin_allowed(pname)
	return minetest.check_player_privs(pname, modname)
end
