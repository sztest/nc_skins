-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs
    = minetest, pairs
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local hands = {}

for pname, texture in pairs(api.all_loaded_skins()) do
	local hname = modname .. ":hand_" .. minetest.sha1(pname)
	hands[pname] = hname
	minetest.register_node(hname, {
			drawtype = "mesh",
			mesh = "nc_player_hand.obj",
			tiles = {texture},
			use_texture_alpha = "clip",
			wield_scale = {x = 2, y = 2, z = 2},
			virtual_item = true,
			paramtype = "light",
			on_punch = minetest.remove_node,
			mapcolor = {a = 0},
		})
end

minetest.register_on_joinplayer(function(player)
		local pname = player:get_player_name()
		local hand = hands[pname:lower()]
		if not hand then return end
		minetest.after(0, function()
				player = minetest.get_player_by_name(pname)
				if not player then return end
				player:get_inventory():set_stack("hand", 1, hand)
			end)
	end)
