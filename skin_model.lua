-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local base = nodecore.player_skin
function nodecore.player_skin(player, options, ...)
	local pname = options and options.playername or player and player:get_player_name()
	local skin = pname and api.get_skin(pname)
	return skin or base(player, options, ...)
end
