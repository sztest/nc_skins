-- LUALOCALS < ---------------------------------------------------------
local include, minetest, rawset
    = include, minetest, rawset
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local api = {}
rawset(_G, modname, api)

include('api_priv')
include('api_rescale')
include('api_skindb')
include('skin_model')
include('skin_wieldhand')
include('notify')
