-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local notify = minetest.settings:get(modname .. "_notify") or ""

if notify == "" then return end

local api = _G[modname]

local pending = {}
local function notifycheck(pname)
	return minetest.after(0, function()
			local player = minetest.get_player_by_name(pname)

			if (not player) or (not nodecore.player_visible(player))
			or pending[pname] or (not api.skin_allowed(pname))
			or api.get_skin(pname) then return end

			pending[pname] = true
			return minetest.after(0, function()
					minetest.chat_send_player(pname, notify)
					pending[pname] = nil
				end)
		end)
end

minetest.register_on_joinplayer(function(player)
		return notifycheck(player:get_player_name())
	end)

local priv = minetest.registered_privileges[modname]
if priv then priv.on_grant = notifycheck end
