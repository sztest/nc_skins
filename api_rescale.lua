-- LUALOCALS < ---------------------------------------------------------
local minetest, tonumber
    = minetest, tonumber
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local function scalecore(skin, suff)
	local scale = tonumber(minetest.settings:get(modname .. "_" .. suff))
	if not (scale and scale > 0) then return skin end
	return skin .. "^[resize:" .. scale .. "x" .. scale
end
function api.rescale(skin)
	if not skin then return skin end
	skin = scalecore(skin, "downscale")
	skin = scalecore(skin, "upscale")
	return skin
end
