### Installing skins

The custom skin for each player needs to be installed into the `textures` folder within this mod (create it if it does not exist).  Files need to be named `nc_skins_player_`*playername*`.png`, where *playername* is the exact name of the player.

**N.B. after installing or changing skins, *the server needs to be restarted* for the changes to take effect**

### Enabling skins

Players' skins will only display if:

- The skin was present at the time the server was started, and
- The player either has the `nc_skins` priv, or the `nc_skins_nopriv` setting is enabled

### Configuration

See [settingtypes.txt](https://gitlab.com/sztest/nc_skins/-/raw/master/settingtypes.txt) for more configuration options.