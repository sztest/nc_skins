-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, pairs
    = minetest, nodecore, pairs
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local skincache = {}

local pref = modname .. "_player_"
local modpath = minetest.get_modpath(modname)
for _, name in pairs(minetest.get_dir_list(modpath .. "/textures", false)) do
	if name:sub(1, #pref) == pref and name:sub(-4) == ".png" then
		local key = name:sub(#pref + 1, -5):lower()
		skincache[key] = api.rescale(name)
	end
end

function api.get_skin(pname)
	if not api.skin_allowed(pname) then return end
	local skin = skincache[pname:lower()]
	if skin and not nodecore.interact(pname) then
		return skin .. "^[mask:nc_player_model_no_interact.png"
	end
	return skin
end
function api.all_loaded_skins()
	return skincache
end
